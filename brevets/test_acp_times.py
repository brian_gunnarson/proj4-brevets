'''
Nosetests for acp_times.py
'''
from acp_times import *
import nose     # testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)


def test_example_1():
    '''
    This is testing example 1 from https://rusa.org/pages/acp-brevet-control-times-calculator
    It's testing correct logic for distances <= 200km

    200km Brevet with controls at 60km, 120km, 175km, and 205km
    Starting Time: Midnight of Jan 1, 2017
    ISO Format of Starting Time: 2017-01-01T00:00
    '''
    start_time = arrow.get("2017-01-01 00:00", "YYYY-MM-DD HH:mm")

    # Open and Close times for 60km
    assert open_time(60, 200, start_time) == "2017-01-01T01:46:00-08:00"
    assert close_time(60, 200, start_time) == "2017-01-01T04:00:00-08:00"

    # Open and Close times for 120km
    assert open_time(120, 200, start_time) == "2017-01-01T03:32:00-08:00"
    assert close_time(120, 200, start_time) == "2017-01-01T08:00:00-08:00"

    # Open and Close times for 175km
    assert open_time(175, 200, start_time) == "2017-01-01T05:09:00-08:00"
    assert close_time(175, 200, start_time) == "2017-01-01T11:40:00-08:00"

    # Open and Close times for 205km
    assert open_time(205, 200, start_time) == "2017-01-01T05:53:00-08:00"
    assert close_time(205, 200, start_time) == "2017-01-01T13:30:00-08:00"


def test_example_3():
    '''
    This is testing example 3 from https://rusa.org/pages/acp-brevet-control-times-calculator
    It's testing correct logic for addition of distances > 200km

    1000km Brevet with a control at 890km
    Starting Time: 11:00am Nov. 15, 2020
    ISO Format of Starting Time: 2020-11-15T11:00
    '''
    start_time = arrow.get("2020-11-15 11:00", "YYYY-MM-DD HH:mm")

    # Make sure the times are not just using one row of the table in calculations
    assert not open_time(890, 1000, start_time) == "2020-11-16T18:47:00-08:00"
    assert not close_time(890, 1000, start_time) == "2020-11-18T16:52:00-08:00"

    # Open and Close time for 890km
    assert open_time(890, 1000, start_time) == "2020-11-16T16:09:00-08:00"
    assert close_time(890, 1000, start_time) == "2020-11-18T04:23:00-08:00"


def test_brevet_boundaries():
    '''
    This is testing brevet boundary points and an obscure time

    1000km Brevet with controls at 200km, 400km, 600km, 1000km
    Starting Time: 2:17pm Dec. 20, 2020
    ISO Format of Starting Time: 2020-12-20T14:17
    '''
    start_time = arrow.get("2020-12-20 14:17", "YYYY-MM-DD HH:mm")

    # Open and Close times for 200km
    assert open_time(200, 1000, start_time) == "2020-12-20T20:10:00-08:00"
    assert close_time(200, 1000, start_time) == "2020-12-21T03:37:00-08:00"

    # Open and Close times for 400km
    assert open_time(400, 1000, start_time) == "2020-12-21T02:25:00-08:00"
    assert close_time(400, 1000, start_time) == "2020-12-21T16:57:00-08:00"

    # Open and Close times for 600km
    assert open_time(600, 1000, start_time) == "2020-12-21T09:05:00-08:00"
    assert close_time(600, 1000, start_time) == "2020-12-22T06:17:00-08:00"

    # Open and Close times for 1000km
    assert open_time(1000, 1000, start_time) == "2020-12-21T23:22:00-08:00"
    assert close_time(1000, 1000, start_time) == "2020-12-23T17:17:00-08:00"


def test_0_distance():
    '''
    This is testing the control distance of 0km

    200km Brevet with control at 0km
    Starting Time: Midnight of Jan 1, 2017
    ISO Format of Starting Time: 2017-01-01T00:0
    '''
    start_time = arrow.get("2017-01-01 00:00", "YYYY-MM-DD HH:mm")

    # Make sure the close time at 0 is not going through a normal calculation
    assert not close_time(0, 200, start_time) == "2017-01-01T00:00:00-08:00"

    # Make sure open and close times for 0km are correct
    assert open_time(0, 200, start_time) == "2017-01-01T00:00:00-08:00"
    assert close_time(0, 200, start_time) == "2017-01-01T01:00:00-08:00"


def test_close_above_boundary():
    '''
    This is testing that a finish point at most 20% greater than the brevet distance is displaying the correct close time.
    Choose 20% greater since the frontend of our app doesn't allow anything >20% to be sent to the server.

    Starting Time: Midnight of Jan 1, 2017
    ISO Format of Starting Time: 2017-01-01T00:0
    '''
    start_time = arrow.get("2017-01-01 00:00", "YYYY-MM-DD HH:mm")

    # Make sure close time 20% greater than 200 doesn't close at calculated time
    assert not close_time(240, 200, start_time) == "2017-01-01T16:00:00-08:00"

    # Make sure close time 20% greater than 400 doesn't close at calculated time
    assert not close_time(480, 400, start_time) == "2017-01-02T08:00:00-08:00"

    # Make sure close time 20% greater than 600 doesn't close at calculated time
    assert not close_time(720, 600, start_time) == "2017-01-03T12:30:00-08:00"

    # Make sure close time 20% greater than 1000 doesn't close at calculated time
    assert not close_time(1200, 1000, start_time) == "2017-01-04T20:30:00-08:00"

    # Make sure 20% greater than 200 displays the close time in the rules
    assert close_time(240, 200, start_time) == "2017-01-01T13:30:00-08:00"

    # Make sure 20% greater than 400 displays the close time in the rules
    assert close_time(480, 400, start_time) == "2017-01-02T03:00:00-08:00"

    # Make sure 20% greater than 400 displays the close time in the rules
    assert close_time(720, 600, start_time) == "2017-01-02T16:00:00-08:00"

    # Make sure 20% greater than 1000 displays the close time in the rules
    assert close_time(1200, 1000, start_time) == "2017-01-04T03:00:00-08:00"