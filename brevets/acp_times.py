"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    table_times = {0: [15, 34], 1: [15, 32], 2: [15, 30], 3: [11.428, 28]}
    # Check if control_dist_km == 0
    if control_dist_km == 0:
        brevet_open_time = brevet_start_time.replace(tzinfo='US/Pacific')
        return brevet_open_time.isoformat()

    # Create a table of brevet distances, excluding 1000
    brevet_distances = [600, 400, 200, 0]

    # Handle if the control_dist is larger than brevet_dist
    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km

    # Split the distance so we can divide by appropriate times
    for distance in brevet_distances:
        if control_dist_km > distance:
            temp = control_dist_km - distance
            leftover_distance = distance
            break

    # Determine how many times we will have to divide and add
    num_arithmetic_moves = int(leftover_distance / 200)

    # Handle the first case with the temporary distance
    result = temp / table_times[num_arithmetic_moves][1]
    num_arithmetic_moves -= 1

    # Handle the rest of the cases with the leftover distance
    while num_arithmetic_moves >= 0:
        result += (200 / table_times[num_arithmetic_moves][1])
        num_arithmetic_moves -= 1

    # Convert our resulting float into hours and minutes
    hours = int(result)
    decimal_minute = (result - hours) * 60
    minutes = round(decimal_minute)

    # Add time to the brevet_start_time and return it in iso format
    brevet_adjusted = brevet_start_time.shift(hours=+hours, minutes=+minutes)
    brevet_end_time = brevet_adjusted.replace(tzinfo='US/Pacific')
    return brevet_end_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
        control_dist_km:  number, the control distance in kilometers
        brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
        brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # Create a table for the set times given in article 9 of the rules
    time_limits = {0: [1, 0], 200: [13, 30], 300: [20, 0], 400: [27, 0], 600: [40, 0], 1000: [75, 0]}

    # 0-600km always divides by 15 so anything above 600 we have to handle separately
    if (control_dist_km > 600) and (brevet_dist_km == 1000):
        temp = control_dist_km - 600
        decimal_total = (600/15) + (temp/11.428)

    # This is the case where control_dist_km is <= 600 so you can always divide by 15
    else:
        decimal_total = control_dist_km/15

    # Calculate hours and minutes
    hours = int(decimal_total)
    decimal_minute = (decimal_total - hours) * 60
    minutes = round(decimal_minute)

    # This handles the special case where we have to deal with time limit rules
    if control_dist_km >= brevet_dist_km or control_dist_km == 0:
        for time in time_limits:
            if control_dist_km >= time:
                hours = time_limits[time][0]
                minutes = time_limits[time][1]


    # Add time to the brevet_start_time and return it in iso format
    brevet_adjusted = brevet_start_time.shift(hours=+hours, minutes=+minutes)
    brevet_end_time = brevet_adjusted.replace(tzinfo='US/Pacific')
    return brevet_end_time.isoformat()
