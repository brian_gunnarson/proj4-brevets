# Brevet Time Calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

In this application, we are essentially replacing the calculator here (https://rusa.org/octime_acp.html).

---

## Author Information

The updated version of this app was created by Brian Gunnarson.

Email: bgunnar5@uoregon.edu

---

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.

---

## Brevet Control Calculation Table

| Control Location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr) |
| :-------------------: | :-------------------: | :-------------------: |
|         0-200         |          15           |          34           |
|        200-400        |          15           |          32           |
|        400-600        |          15           |          30           |
|       600-1000        |        11.428         |          28           |

**Possible Brevet Distances (km):** 200, 400, 600, or 1000

Calculations for open and close times at control points are based on the table (from https://rusa.org/pages/acp-brevet-control-times-calculator) and the possible brevet distances listed above.

---

## Open Times:

Open times are calculated using maximum speed in the Brevet Control Calculation section above.

#### Less than 200km

Given a control location less than 200km, we can use the control location and the maximum speed to calculate a time measured in hours:

_Total = control location / maximum speed_

_**Example:** Consider a brevet of 200km with a control point at 100km. To calculate the opening time you would use:_

_Total = 153/34 = 4.5_

To get the hours for this time you need to take the floor value of the total.

To figure out the minutes you need to subtract the whole number of hours from the total (numbers to the left of the decimal), multiply the resulting fractional part (numbers to the right of the decimal) by 60, and round to the nearest integer. Using the same example:

_Hours = floor(4.5) = 4_

_Minutes = round(60 x (4.5-4)) = 30_

_So a control point at 100km should open 4H30M after the initial start of the brevet._

#### Greater than 200km

For control distances greater than 200km, we must take into consideration the max speed in the current row and the max speed in each of the preceding rows of the table. For instance, a 400km brevet must take the first two rows into account, a 600km brevet must take the first three rows into account, and a 1000km brevet must take all rows into account.

_**Example 1:** Consider a 400km brevet with a control point at 305km._

_To start, we consider the first 200km (aka the first row): 200/34 = 5H53M_

_Then we consider the leftover distance of 150km (obtained from the subtraction: 350km-200km): 150/32 = 4H41M_

_Finally, we add the two times together: 5H53M + 4H41M = 10H34M_

_So a control point at 350km should open 10H34M after the initial start of the brevet._

_**Example 2:** Consider a 1000km brevet with a control point at 890km._

_To start, we consider the first 3 rows: 200/34 + 200/32 + 200/30 = 5H53M + 6H15M + 6H40M = 18H48M_

_Then we consider the leftover distance of 290km (890km-600km): 290/28 = 10H21M_

_Finally, we add the two times together: 18H48M + 10H21M = 29H09M_

_So a control point at 890km should open 29H09M after the initial start of the brevet._

#### Boundary Distances

For control points at the exact distances of 200km, 400km, and 600km use the speeds at the highest row in the table:

| Control Point (km) | Maximum Speed (km/hr) |
| :----------------: | :-------------------: |
|        200         |          34           |
|        400         |          32           |
|        600         |          30           |

---

## Close Times:

Close times are calculated using minimum speed in the Brevet Control Calculation section above.

#### Less than 600km

For control points less than 600km\*, the minimum speed is always 15. Therefore our calculation for this case looks like:

_Total = control location / 15_

From here, we convert to hours and minutes the same way we did for opening times.

_**Example:** Consider a 600km brevet with a control point at 475km._

_Total = 475/15 = 31.6666..._

_Hours = floor(31.6666...) = 31_

_Minutes = round(60 x (31.6666...-31)) = 40_

_So a control point at 475km should close 31H40M after the initial start of the brevet._

\*<font size=1>See the special cases section below for control points of exactly 0km, 200km, 400km, and 600km</font>

#### Greater than 600km

For control points greater than 600km, we use the same calculation method as we did for opening times greater than 200km.

_**Example:** Consider a 1000km brevet with a control point at 890km._

_We initially consider the first 600km: 600/15 = 40H00M_

_Then we consider the last 290km (890km - 600km): 290/11.428 = 25H23M_

_Finally, we add the two times together: 40H00M + 25H23M = 65H23M_

_So a control point at 890km should close 65H23M after the initial start of the brevet._

---

## Special Closing Time Cases

It's important to keep a couple rules about closing times in mind (from article 9 at https://rusa.org/pages/rulesForRiders):

| Distance (km) | Closing Times (HH:mm) |
| :-----------: | :-------------------: |
|       0       |         1:00          |
|      200      |         13:30         |
|      300      |         20:00         |
|      400      |         27:00         |
|      600      |         40:00         |
|     1000      |         75:00         |

If a control point is specified as one of the distances in the table above, then the corresponding closing time will always be used. For example, consider a 600km brevet with control points at 200km, 250km, and 350km. The close times at 250km and 350km will be calculated like normal but the close time at 200km will always be 13 hours and 30 minutes after the start of the brevet (from the table above).

For the special control point at 0, it's explained in the rule oddities that the close time will be exactly 1 hour after the start of the brevet. This means that any control point less than 15km will close before the control point of 0.

Additionally, in this application you cannot enter a control point 20% greater than the brevet distance. For example, in a 200km brevet the largest control point you can enter is 240km (200km \* 1.2).

For any control point greater than the brevet distance and less than 20% greater than the brevet distance, the specified closed times in the table above will be used. For example, in a 200km brevet, any control point between 200km and 240km will close 13 hours and 30 minutes after the initial start of the brevet.

---

## Program Specifics

In this app we use AJAX and JavaScript in the front end, python and flask in the backend, and nosetests to test our acp_times.py file.
